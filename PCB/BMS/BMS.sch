EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Battery_Management:BQ76920PW U?
U 1 1 5FF4093C
P 2600 2350
F 0 "U?" H 2600 3331 50  0000 C CNN
F 1 "BQ76920PW" H 2600 3240 50  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 3500 1600 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/bq76920.pdf" H 3300 2900 50  0001 C CNN
	1    2600 2350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
