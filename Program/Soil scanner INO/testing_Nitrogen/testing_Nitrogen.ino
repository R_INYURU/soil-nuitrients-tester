 #include <SoftwareSerial.h>

SoftwareSerial mod(9,10); //RO, DI

#define RE 5
#define DE 8

const byte NITRO[]  = {0x01, 0x03, 0x00, 0x1e, 0x00, 0x01, 0xe4, 0x0c};
 
byte values[11];
byte checkByte = 0;
int repeats = 0;
int dataSolidity = 0;

int MBcode_check(byte *MBcode, int size)
{
  if (MBcode[0] != 1 && MBcode[1] != 3 &&
      MBcode[2] != 2 && MBcode[3] != 0)
  {
    return -1;  		// decrement the sampling count by 1
  } else {
    if (dataSolidity == 0 && checkByte == 0)
    {
      checkByte = MBcode[4];
      dataSolidity = 1;
      return 0;			// Continue sampling
      
    } else if (dataSolidity > 0 && checkByte == MBcode[4]) {
      dataSolidity ++;
      return 0;			// Continue sampling
      
    } else if (dataSolidity > 0 && checkByte != MBcode[4]) {
      repeats ++;
      if (repeats == 10)
	      Serial.println("repeats FULL");
        return 1;		// Cancel/Restart sampling
    }
  }
}

int Nitro_sampling()
{
	int sampleRange = 70;
	int samples[sampleRange] = {0},
			SampleAvg = 0,
			TtlSample = 0;
	int size = 7;
	bool done = false;
	
  for (int s=0; s<sampleRange; s++)
  {
		byte *nitro = Nitrogen();
/*		Serial.print(" [");*/
/*		Serial.print(s);*/
/*		Serial.println("]");*/
		if (MBcode_check(nitro, size) == -1)
		{
			s--;
			done = false;
			delay(30);
			continue;
		} else if (MBcode_check(nitro, size) == 1){
			done = false;
			delay(30);
			break;
		} else {
		  samples[s] = nitro[4];
		  done = true;
		  delay(30);
		}
  }

	checkByte = 0;
	repeats = 0;
	dataSolidity = 0;
	
	if (done)
	{
		for (int i=0; i<sampleRange; i++)
		{
		  TtlSample = TtlSample + samples[i];
		}
/*		Serial.print("Sampled total: ");*/
/*		Serial.println(TtlSample);*/
		return SampleAvg = TtlSample / sampleRange;
	} else {
		Nitro_sampling();
	}
}
 
byte *Nitrogen()
{
  digitalWrite(DE, HIGH);
  digitalWrite(RE, HIGH);
  delay(10);
  
  if(mod.write(NITRO, sizeof(NITRO)) == 8)
  {
    digitalWrite(DE, LOW);
    digitalWrite(RE, LOW);
    for(byte i=0; i<7; i++)
    {
      values[i] = mod.read();
      Serial.print(values[i], HEX);
      Serial.print(" ");
    }
    Serial.print(" > ");
  }
  
  return values;
}

void setup() {
  Serial.begin(115200);
  mod.begin(9600);
  
  pinMode(RE, OUTPUT);
  pinMode(DE, OUTPUT);
}
 
void loop() {
	int selector = 1;
	
	if (selector == 0)
	{
		byte *val1;

		Serial.print("Hex: ");
		val1 = Nitrogen();
		Serial.print("Val: ");
		Serial.print(val1[4]);
		Serial.println(" mg/Kg");
		delay(50);
	} else if (selector == 1) {
		int val = Nitro_sampling();
		Serial.print("Nitro avg: ");
		Serial.print(val);
		Serial.println(" mg/Kg");
	}

  delay(500);
}
