 #include <SoftwareSerial.h>

SoftwareSerial NPKmb(9,10); //RO, DI

#define RE 5
#define DE 8

const byte NITRO[]  = {0x01, 0x03, 0x00, 0x1e, 0x00, 0x01, 0xe4, 0x0c};
const byte PHOSP[]   = {0x01, 0x03, 0x00, 0x1f, 0x00, 0x01, 0xb5, 0xcc};
const byte POTAS[]   = {0x01, 0x03, 0x00, 0x20, 0x00, 0x01, 0x85, 0xc0};
 
byte MB_register[8] = {0};
byte checkByte[3] = {0};
int repeats = 0;
int dataSolidity = 0;
int sampleRange = 40;

byte *Nitrogen()
{
  digitalWrite(DE, HIGH);
  digitalWrite(RE, HIGH);
  delay(10);
  
  if(NPKmb.write(NITRO, sizeof(NITRO)) == 8)
  {
    digitalWrite(DE, LOW);
    digitalWrite(RE, LOW);
    for(byte i=0; i<7; i++)
    {
      MB_register[i] = NPKmb.read();
//      Serial.print(MB_register[i], HEX);
//      Serial.print(" ");
    }
//    Serial.print("     ");
  }
  
  return MB_register;
}
 
byte *Phosphorous()
{
  digitalWrite(DE, HIGH);
  digitalWrite(RE, HIGH);
  delay(10);
  
  if(NPKmb.write(PHOSP, sizeof(PHOSP)) == 8)
  {
    digitalWrite(DE, LOW);
    digitalWrite(RE, LOW);
    for(byte i=0; i<7; i++)
    {
		  MB_register[i] = NPKmb.read();
/*		  Serial.print(MB_register[i], HEX);*/
/*		  Serial.print(" ");*/
    }
/*    Serial.print("     ");*/
  }
  
  return MB_register;
}
 
byte *Potassium()
{
  digitalWrite(DE, HIGH);
  digitalWrite(RE, HIGH);
  delay(10);
  
  if(NPKmb.write(POTAS, sizeof(POTAS)) == 8)
  {
    digitalWrite(DE, LOW);
    digitalWrite(RE, LOW);
    for(byte i=0; i<7; i++)
    {
		  MB_register[i] = NPKmb.read();
/*		  Serial.print(MB_register[i], HEX);*/
/*		  Serial.print(" ");*/
    }
/*    Serial.println();*/
  }
  
  return MB_register;
}

int MBcode_check(byte *MBcode, int size)
{
  if (MBcode[0] != 1 || MBcode[1] != 3 ||
      MBcode[2] != 2 || MBcode[3] != 0)
  {
    return -1;      // decrement the sampling count by 1
  } else {
    if (dataSolidity == 0 && (checkByte[0] == 0 || checkByte[1] == 0 || checkByte[2]) )
    {
      dataSolidity ++;
      if (checkByte[0] == 0)
      {
        checkByte[0] = MBcode[4];
      } else if (checkByte[1] == 0) {
        checkByte[1] = MBcode[4];
      } else if (checkByte[2] == 0) {
        checkByte[2] = MBcode[4];
      }
      return 0;     // Continue sampling
      
    } else if (dataSolidity > 0 && (checkByte[0] == MBcode[4] || 
                                    checkByte[1] == MBcode[4] || 
                                    checkByte[2] == MBcode[4]) ) {
      dataSolidity ++;
      return 0;     // Continue sampling
      
    } else if (dataSolidity > 0 && (checkByte[0] != MBcode[4] || 
                                    checkByte[1] != MBcode[4] || 
                                    checkByte[2] != MBcode[4]) ) {
      repeats ++;
      if (repeats == 10)
        return 1;   // Cancel/Restart sampling
    }
  }
}

int Nitro_sampling()
{
	int samples[sampleRange] = {0},
			SampleAvg = 0,
			TtlSample = 0;
	int size = 7;
	bool done = false;
	
  for (int s=0; s<sampleRange; s++)
  {
		byte *nitro = Nitrogen();

		if (MBcode_check(nitro, size) == -1)
		{
			s--;
			done = false;
			delay(30);
			continue;
		} else if (MBcode_check(nitro, size) == 1){
			done = false;
			delay(30);
			break;
		} else {
		  samples[s] = nitro[4];
		  done = true;
		  delay(30);
		}
  }

  checkByte[3] = {0};
	repeats = 0;
	dataSolidity = 0;
	
	if (done)
	{
		for (int i=0; i<sampleRange; i++)
		{
		  TtlSample = TtlSample + samples[i];
		}
		
		return SampleAvg = TtlSample / sampleRange;
	} else {
		Nitro_sampling();
	}
}

int Phosp_sampling()
{
	int samples[sampleRange] = {0},
			SampleAvg = 0,
			TtlSample = 0;
	int size = 7;
	bool done = false;
	
  for (int s=0; s<sampleRange; s++)
  {
		byte *phosp = Phosphorous();
		
		if (MBcode_check(phosp, size) == -1)
		{
			s--;
			done = false;
			delay(30);
			continue;
		} else if (MBcode_check(phosp, size) == 1){
			done = false;
			delay(30);
			break;
		} else {
		  samples[s] = phosp[4];
		  done = true;
		  delay(30);
		}
  }

  checkByte[3] = {0};
	repeats = 0;
	dataSolidity = 0;
	
	if (done)
	{
		for (int i=0; i<sampleRange; i++)
		{
		  TtlSample = TtlSample + samples[i];
		}
		
		return SampleAvg = TtlSample / sampleRange;
	} else {
		Phosp_sampling();
	}
}

int Potas_sampling()
{
	int samples[sampleRange] = {0},
			SampleAvg = 0,
			TtlSample = 0;
	int size = 7;
	bool done = false;
	
  for (int s=0; s<sampleRange; s++)
  {
		byte *potas = Potassium();
		
		if (MBcode_check(potas, size) == -1)
		{
			s--;
			done = false;
			delay(30);
			continue;
		} else if (MBcode_check(potas, size) == 1){
			done = false;
			delay(30);
			break;
		} else {
		  samples[s] = potas[4];
		  done = true;
		  delay(30);
		}
  }

  checkByte[3] = {0};
	repeats = 0;
	dataSolidity = 0;
	
	if (done)
	{
		for (int i=0; i<sampleRange; i++)
		{
		  TtlSample = TtlSample + samples[i];
		}
		
		return SampleAvg = TtlSample / sampleRange;
	} else {
		Potas_sampling();
	}
}

void setup() {
  Serial.begin(115200);
  NPKmb.begin(9600);
  pinMode(RE, OUTPUT);
  pinMode(DE, OUTPUT);
  
  Serial.println("*************************************************************");
  Serial.println("***          Soil nutrient sensor (NPK)[mg/kg]            ***");
  Serial.println("*************************************************************");
  Serial.println("        Nitrogen            Phosphorous             Potassium");
}
 
void loop() {
	int selector = 1;

	if (selector == 0)
	{
		byte *val1, *val2, *val3;

		Serial.print("Hex: ");
		val1 = Nitrogen();
		delay(50);
		val2 = Phosphorous();
		delay(50);
		val3 = Potassium();
		delay(50);
	
		Serial.print("Val:        ");
		Serial.print(val1[4]);
		Serial.print("                    ");
		Serial.print(val2[4]);
		Serial.print("                    ");
		Serial.println(val3[4]);
	} else if (selector == 1) {
		int val1, val2, val3;
		
		val1 = Nitro_sampling();
		delay(10);
		val2 = Phosp_sampling();
		delay(10);
		val3 = Potas_sampling();
		delay(10);
	
		Serial.print("Val:        ");
		Serial.print(val1);
		Serial.print("                    ");
		Serial.print(val2);
		Serial.print("                    ");
		Serial.println(val3);
	}
  
  delay(100);
}
