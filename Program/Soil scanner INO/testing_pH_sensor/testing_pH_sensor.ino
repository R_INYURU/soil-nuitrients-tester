#include <SoftwareSerial.h>

#define ph_RE 6
#define ph_DE 7

SoftwareSerial PHmb(3, 4);

const byte PH_addr[] = {0x01, 0x03, 0x00, 0x0D, 0x00, 0x01, 0x15, 0xC9};
byte MB_register[8] = {0};
byte checkByte[3] = {0};
int repeats = 0;
int dataSolidity = 0;
int sampleRange = 50;

byte *pH_val()
{
  digitalWrite(ph_DE, HIGH);
  digitalWrite(ph_RE, HIGH);
  delay(10);
  
  if(PHmb.write(PH_addr, sizeof(PH_addr)) == 8)
  {
    digitalWrite(ph_DE, LOW);
    digitalWrite(ph_RE, LOW);
    for(byte i=0; i<7; i++)
    {
      MB_register[i] = PHmb.read();
      Serial.print(MB_register[i], HEX);
      Serial.print(" ");
    }
//    Serial.println();
  }
  
  return MB_register;
}

int MBcode_check(byte *MBcode, int size)
{
  if (MBcode[0] != 1 || MBcode[1] != 3 ||
      MBcode[2] != 2 || MBcode[3] != 0)
  {
    return -1;      // decrement the sampling count by 1
  } else {
    if (dataSolidity == 0 && (checkByte[0] == 0 || checkByte[1] == 0 || checkByte[2]) )
    {
      dataSolidity ++;
      if (checkByte[0] == 0)
      {
        checkByte[0] = MBcode[4];
      } else if (checkByte[1] == 0) {
        checkByte[1] = MBcode[4];
      } else if (checkByte[2] == 0) {
        checkByte[2] = MBcode[4];
      }
      return 0;     // Continue sampling
      
    } else if (dataSolidity > 0 && (checkByte[0] == MBcode[4] || 
                                    checkByte[1] == MBcode[4] || 
                                    checkByte[2] == MBcode[4]) ) {
      dataSolidity ++;
      return 0;     // Continue sampling
      
    } else if (dataSolidity > 0 && (checkByte[0] != MBcode[4] || 
                                    checkByte[1] != MBcode[4] || 
                                    checkByte[2] != MBcode[4]) ) {
      repeats ++;
      if (repeats == 10)
        return 1;   // Cancel/Restart sampling
    }
  }
}

float PH_sampling()
{
	int samples[sampleRange] = {0},
			SampleAvg = 0,
			TtlSample = 0;
	int size = 7;
	bool done = false;
	
  for (int s=0; s<sampleRange; s++)
  {
		byte *pH = pH_val();
    Serial.print(" [");
    Serial.print(s);
    Serial.println("]");

		if (MBcode_check(pH, size) == -1)
		{
			s--;
			done = false;
			delay(30);
			continue;
		} else if (MBcode_check(pH, size) == 1){
			done = false;
			delay(30);
			break;
		} else {
		  samples[s] = pH[4];
		  done = true;
		  delay(30);
		}
  }

	checkByte[3] = {0};
	repeats = 0;
	dataSolidity = 0;
	
	if (done)
	{
		for (int i=0; i<sampleRange; i++)
		{
		  TtlSample = TtlSample + samples[i];
		}
		SampleAvg = TtlSample / sampleRange;
   
		return (float) SampleAvg / 10;
	} else {
		PH_sampling();
	}
}

void setup() {
  Serial.begin(115200);
  PHmb.begin(9600);

  pinMode(ph_RE, OUTPUT);
  pinMode(ph_DE, OUTPUT);
}

void loop() {
  byte *val;
  float Val;

//  Serial.print("Hex: ");
//  val = pH_val();
//  delay(25);
//  
//  Serial.print("Val: ");
//  Serial.println(val[4]);

  Val = PH_sampling();
  delay(25);
  Serial.println(Val);
  delay(1000);
}
