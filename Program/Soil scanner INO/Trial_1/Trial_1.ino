#include <Sim800L.h>
#include <SoftwareSerial.h>
#include <TinyGPS.h>
#include <dhtnew.h>
#include <U8g2lib.h>

#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

#define GPS_RX  4
#define GPS_TX  3

#define GSM_RX  10
#define GSM_TX  11

U8G2_SSD1306_128X64_ALT0_1_HW_I2C u8g2(U8G2_R0, U8X8_PIN_NONE);

//Sim800L GSM(GSM_RX, GSM_TX);

TinyGPS gps;
SoftwareSerial ss(GPS_RX, GPS_TX);

DHTNEW DHTsensor(A0);

uint8_t pageNum = 0;

uint8_t day, month, year, minute, second, hour;

struct Temp {
  float Cels;
  float Fahr;
  float Hum;
};

static void smart_delay (unsigned long ms)
{
  unsigned long start = millis();
  do 
  {
    while (ss.available())
      gps.encode(ss.read());
  } while (millis() - start < ms);
}

struct Temp get_DHT_data ()
{
  struct Temp temp;
  
 if (millis() - DHTsensor.lastRead() > 3000)
  {
    DHTsensor.read();
    temp.Cels = DHTsensor.getTemperature();
    temp.Fahr = (temp.Cels * 1.8000) + 32.00;
    
    temp.Hum = DHTsensor.getHumidity();
  }

  return temp;
}

void GPS_data (float lat, float lon, long alt, float invalid, long altInvalid)
{
  uint8_t _x1 = 2;
  uint8_t _x2 = 116;
  uint8_t _x3 = 24;
  
  char Lat[10];
  char Lon[10];
  char Alt[10];
  
  dtostrf(lat, 2, 6, Lat);
  dtostrf(lon, 2, 6, Lon);
  ltoa(alt, Alt, 10);
  
  u8g2.setFont(u8g2_font_5x7_mf);
  u8g2.drawStr(40, 8, "GPS Data");
  u8g2.drawStr(_x1, 16, "Lon:");   // -1.944528
  u8g2.drawStr(_x1, 24, "Lat:");   // 30.089500
  u8g2.drawStr(_x1, 32, "Alt:");   // 156700
  // Draw page indicator
  u8g2.drawDisc(_x2, 36, 2);
  u8g2.drawCircle(_x2, 12, 2);
  u8g2.drawCircle(_x2, 20, 2);
  u8g2.drawCircle(_x2, 28, 2);
  
  if (lat == invalid && lon == invalid && alt == altInvalid)
  {
    u8g2.setCursor(_x3, 16);
    u8g2.print("*********");
    u8g2.setCursor(_x3, 24);
    u8g2.print("*********");
    u8g2.setCursor(_x3, 32);
    u8g2.print("*********");
  }
  else
  {
    u8g2.setCursor(_x3, 16);
    u8g2.print(Lon);
    u8g2.setCursor(_x3, 24);
    u8g2.print(Lat);
    u8g2.setCursor(_x3, 32);
    u8g2.print(Alt);
  }
  
  smart_delay(0);
}

void GSM_data ()
{
  uint8_t _x2 = 116;
  
  day = 3;
  month = 2;
  year = 2021;
  hour = 13;
  minute = 14;
  second = 44;
  //  GSM.RTCtime(&day, &month, &year, &hour, &minute, &second);
  
  u8g2.setFont(u8g2_font_5x7_mf);
  u8g2.drawStr(40, 8, "GSM Time");
  u8g2.drawStr(35, 16, "-           -");
  u8g2.drawStr(2, 24, "Date: ");
  u8g2.drawStr(2, 32, "Time: ");

  
  // Draw page indicator
  u8g2.drawDisc(_x2, 12, 2);
  u8g2.drawCircle(_x2, 36, 2);
  u8g2.drawCircle(_x2, 20, 2);
  u8g2.drawCircle(_x2, 28, 2);
}

void Temp_sensor ()
{
  uint8_t _x2 = 116;
  struct Temp temp = get_DHT_data();
  
  u8g2.setFont(u8g2_font_5x7_mf);
  u8g2.drawStr(32, 7, "Temp sensor");
  u8g2.drawStr(2, 16, "Temperature");
  u8g2.drawStr(33, 24, "C");
  u8g2.drawStr(76, 24, "F");
  u8g2.drawStr(2, 31, "Humidity: ");
  u8g2.drawStr(74, 32, "%");
  u8g2.setFont(u8g2_font_unifont_t_symbols);
  u8g2.drawGlyph(26, 27, 0x00b0);
  u8g2.drawGlyph(69, 27, 0x00b0);

  u8g2.setFont(u8g2_font_5x7_mf);
  u8g2.setCursor(6, 24);
  u8g2.print(temp.Cels, 1);
  u8g2.setCursor(48, 24);
  u8g2.print(temp.Fahr, 1);
  u8g2.setCursor(52, 32);
  u8g2.print(temp.Hum, 1);
  
  // Draw page indicator
  u8g2.drawDisc(_x2, 20, 2);
  u8g2.drawCircle(_x2, 12, 2);
  u8g2.drawCircle(_x2, 36, 2);
  u8g2.drawCircle(_x2, 28, 2);
  
}

void SD_card ()
{
  uint8_t _x2 = 116;
  
  u8g2.setFont(u8g2_font_5x7_mf);
  u8g2.drawStr(40, 8, "SD card");
  u8g2.drawStr(35, 16, "");
  u8g2.drawStr(2, 24, "");
  u8g2.drawStr(2, 32, "");

  
  // Draw page indicator
  u8g2.drawDisc(_x2, 28, 2);
  u8g2.drawCircle(_x2, 12, 2);
  u8g2.drawCircle(_x2, 20, 2);
  u8g2.drawCircle(_x2, 36, 2);
}

void Draw_page(uint8_t page)
{
  float fLat, fLon;
  long Alt;
  switch (page) {
    case 0: 
        gps.f_get_position(&fLat, &fLon);
        Alt = gps.altitude();
        GPS_data(fLat, fLon, Alt, TinyGPS::GPS_INVALID_F_ANGLE, TinyGPS::GPS_INVALID_ALTITUDE);
      break;
      
    case 1: 
        GSM_data();
      break;
      
    case 2:
        Temp_sensor();
      break;
      
    case 3:
        SD_card();
      break;
      
    default:
      break;
  }
}

void setup() {
  u8g2.begin();
  ss.begin(9600);
  //  GSM.begin(4800);
  
  //  Serial.begin(115200);

  DHTsensor.setHumOffset(7);
  DHTsensor.setTempOffset(-5);
}

void loop()
{
  // page loop
  u8g2.firstPage();  
  do {
    Draw_page(pageNum);
  } while( u8g2.nextPage() );
  
  pageNum++;
  if ( pageNum > 3)
    pageNum = 0;

  //delay between pages & refresh GPS
  smart_delay(5000);
}
