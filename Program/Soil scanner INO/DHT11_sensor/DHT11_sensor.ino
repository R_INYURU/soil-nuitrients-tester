#include <dhtnew.h>

DHTNEW DHTsensor(A0);


void setup() {
  Serial.begin(115200);

  DHTsensor.setHumOffset(7);
  DHTsensor.setTempOffset(-5);

  Serial.println("Humidity    Temperature");
  Serial.println("  %          °C      °F");
  Serial.println("-----------------------");
}

void loop() 
{
  if (millis() - DHTsensor.lastRead() > 2000)
  {
    DHTsensor.read();
    float CelTemp = DHTsensor.getTemperature();
    float HumVal = DHTsensor.getHumidity();
    Serial.print(" ");
    Serial.print(HumVal, 1);
    Serial.print("       ");
    Serial.print(CelTemp, 1);
    Serial.print("   ");
    Serial.println(calcFahr(CelTemp), 1);
  }

}

float calcFahr(float cel)
{
  float degrees;

  degrees = ( cel * 1.8000 ) + 32.00;
  
  return degrees;
}
