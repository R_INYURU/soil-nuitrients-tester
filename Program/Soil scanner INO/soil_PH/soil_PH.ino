#include <SoftwareSerial.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define ph_RE 6
#define ph_DE 7
#define TEMP_SENSOR 2

SoftwareSerial PHmodbus(3, 4);  //RO, DI
OneWire oneWire(TEMP_SENSOR);
DallasTemperature sensors(&oneWire);

uint8_t onetime = 0;
uint8_t dataSolidity = 0;
uint8_t tolerance = 0;
byte desiredByte = 0;

float PH_sensor ()
{
  const byte PH_acid[] = {0x01, 0x03, 0x00, 0x0D, 0x00, 0x01, 0x15, 0xC9};
  const byte PH_base[] = {0x01, 0x03, 0x00, 0x06, 0x00, 0x01, 0x64, 0x0B};
  float soil_ph = 0.0;
  byte MB_register[8];

  digitalWrite(ph_DE, HIGH);
  digitalWrite(ph_RE, HIGH);
  delay(10);

  if (PHmodbus.write(PH_acid, sizeof(PH_acid)) == 8)
  {
    digitalWrite(ph_DE, LOW);
    digitalWrite(ph_RE, LOW);

    for (byte i=0; i<7; i++)
    {
      MB_register[i] = PHmodbus.read();
      Serial.print(MB_register[i], HEX);
      Serial.print(" ");
    }
    Serial.println();

    if (MB_register[0] != 1 &&
        MB_register[1] != 3 &&
        MB_register[2] != 2 &&
        MB_register[3] != 0)
    {
      return  -1;   // decrement the sample taken by 1
    } else {
      if (dataSolidity == 0 && desiredByte == 0)
      {
        desiredByte = MB_register[4];
        dataSolidity = 1;
        soil_ph = float(MB_register[3] + MB_register[4]) / 10;

        return soil_ph;
      } else if (dataSolidity > 0 && desiredByte == MB_register[4]) {
        dataSolidity ++;
        soil_ph = float(MB_register[3] + MB_register[4]) / 10;

        return soil_ph;
      } else if (dataSolidity > 0 && desiredByte != MB_register[4]) {
        tolerance ++;
        if (tolerance == 10)
          return 0;   // cancel the samples taken
          
        soil_ph = float(MB_register[3] + MB_register[4]) / 10;

        return soil_ph;
      }
    }
  }
}

float soil_temp()
{
  sensors.requestTemperatures();
  float tempC = sensors.getTempCByIndex(0);

  if (tempC != DEVICE_DISCONNECTED_C)
  {
//    Serial.print("temp: ");
//    Serial.println(tempC);

    return tempC;
  }
}

void setup() {
  Serial.begin(115200);
  PHmodbus.begin(9600);
  sensors.begin();
  
  pinMode(ph_RE, OUTPUT);
  pinMode(ph_DE, OUTPUT);
}

void loop() {
  uint8_t sampleRange = 50;
  float phSamples[sampleRange],
        phSampleAvg = 0.0,
        phTtlSamples = 0.0,
        soilPH = 0.0;
  
  float tempSamples[sampleRange], 
        tempSampleAvg = 0.0, 
        TempTtlSamples = 0.0, 
        soilTemp = 0.0;
/* Calculating average Soil PH */
  for (uint8_t s=0; s<sampleRange; s++)
  {
    Serial.print(s);
    Serial.print(") ");
    soilPH = PH_sensor();
    if (soilPH == -1)
    {
      s--;    // do not count this iteration
      delay(30);
      continue;
    }  else if (soilPH == 0){
      delay(30);
      return 0;
    }
    phSamples[s] = soilPH;
    delay(30);
  }

  for (uint8_t i=0; i<sampleRange; i++)
  {
    phTtlSamples = phTtlSamples + phSamples[i];
  }

  phSampleAvg = phTtlSamples / sampleRange;

  dataSolidity = 0;
  tolerance = 0;
  desiredByte = 0;

/* Calculating average of Soil Temperature */
//  for (uint8_t s=0; s<sampleRange; s++)
//  {
//    soilTemp= soil_temp();
//    tempSamples[s] = soilTemp;
//    delay(30);
//  }
//
//  for (uint8_t i=0; i<sampleRange; i++)
//  {
//    TempTtlSamples = TempTtlSamples + tempSamples[i]; 
//  }
//  
//  tempSampleAvg = TempTtlSamples / sampleRange;
  
/* Displaying values of Soil PH and Temperature */
  if (!onetime)
  {
    Serial.println("**************************************************");
    Serial.println("*    Soil Acidity/Alkalinity and Temperature     *");
    Serial.println("**************************************************");
    Serial.println("        Soil PH             Soil temperature");
    onetime = 1;
  }
  Serial.print("        ");
  Serial.print(phSampleAvg);
  Serial.print("                   ");
  Serial.println(tempSampleAvg);

  delay(1000);
}
