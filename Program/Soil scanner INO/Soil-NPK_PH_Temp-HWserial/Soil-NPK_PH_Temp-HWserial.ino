/********************************************************************
 * Not all pins on the Mega and Mega 2560 support change interrupts,
 * so only the following can be used for RX:
 * 10, 11, 12, 13, 50, 51, 52, 53, 62, 63, 64, 65, 66, 67, 68, 69
 * 
 *******************************************************************/

#include <SoftwareSerial.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <GSMSimHTTP.h>

#define TEMP_SENSOR 14
#define npk_RE      3
#define npk_DE      2
#define ph_RE       5
#define ph_DE       4
#define GSM_RST     22

SoftwareSerial PHmb(12, 9);   //RO, DI
SoftwareSerial NPKmb(11, 8);    //RO, DI

OneWire oneWire(TEMP_SENSOR);
DallasTemperature sensors(&oneWire);
GSMSimHTTP http(Serial1, GSM_RST);

const byte PH_addr[] = {0x01, 0x03, 0x00, 0x0D, 0x00, 0x01, 0x15, 0xC9};
const byte NITRO[]  = {0x01, 0x03, 0x00, 0x1e, 0x00, 0x01, 0xe4, 0x0c};
const byte PHOSP[]  = {0x01, 0x03, 0x00, 0x1f, 0x00, 0x01, 0xb5, 0xcc};
const byte POTAS[]  = {0x01, 0x03, 0x00, 0x20, 0x00, 0x01, 0x85, 0xc0};

const char URL[] = "api.thingspeak.com/update?api_key=2W5WQS9K4J8R5KBX";

byte MB_register[8] = {0};
byte checkByte[3] = {0};
int repeats = 0;
int dataSolidity = 0;
int sampleRange = 50;

float Temp()
{
  sensors.requestTemperatures();
  float tempC = sensors.getTempCByIndex(0);

  if (tempC != DEVICE_DISCONNECTED_C)
  {
    return tempC;
  }
  tempC = 0;
  
  return tempC;
}

byte *Nitrogen()
{
  digitalWrite(npk_DE, HIGH);
  digitalWrite(npk_RE, HIGH);
  delay(10);

  if(NPKmb.write(NITRO, sizeof(NITRO)) == 8)
  {
    digitalWrite(npk_DE, LOW);
    digitalWrite(npk_RE, LOW);
    for(byte i=0; i<7; i++)
    {
      MB_register[i] = NPKmb.read();
      // Serial.print(MB_register[i], HEX);
      // Serial.print(" ");
    }
    // Serial.println();
  }
  
  return MB_register;
}
 
byte *Phosphorous()
{
  digitalWrite(npk_DE, HIGH);
  digitalWrite(npk_RE, HIGH);
  delay(10);
  
  if(NPKmb.write(PHOSP, sizeof(PHOSP)) == 8)
  {
    digitalWrite(npk_DE, LOW);
    digitalWrite(npk_RE, LOW);
    for(byte i=0; i<7; i++)
    {
      MB_register[i] = NPKmb.read();
      // Serial.print(MB_register[i], HEX);
      // Serial.print(" ");
    }
    // Serial.println();
  }
  
  return MB_register;
}
 
byte *Potassium()
{
  digitalWrite(npk_DE, HIGH);
  digitalWrite(npk_RE, HIGH);
  delay(10);
  
  if(NPKmb.write(POTAS, sizeof(POTAS)) == 8)
  {
    digitalWrite(npk_DE, LOW);
    digitalWrite(npk_RE, LOW);
    for(byte i=0; i<7; i++)
    {
      MB_register[i] = NPKmb.read();
      // Serial.print(MB_register[i], HEX);
      // Serial.print(" ");
    }
    // Serial.println();
  }
  
  return MB_register;
}

byte *pH_val()
{
  digitalWrite(ph_DE, HIGH);
  digitalWrite(ph_RE, HIGH);
  delay(10);
  
  if(PHmb.write(PH_addr, sizeof(PH_addr)) == 8)
  {
    digitalWrite(ph_DE, LOW);
    digitalWrite(ph_RE, LOW);
    for(byte i=0; i<7; i++)
    {
      MB_register[i] = PHmb.read();
      // Serial.print(MB_register[i], HEX);
      // Serial.print(" ");
    }
    // Serial.println();
  }
  
  return MB_register;
}

int MBcode_check(byte *MBcode, int size)
{
  if (MBcode[0] != 1 || MBcode[1] != 3 ||
      MBcode[2] != 2 || MBcode[3] != 0)
  {
    return -1;      // decrement the sampling count by 1
  } else {
    if (dataSolidity == 0 && (checkByte[0] == 0 || checkByte[1] == 0 || checkByte[2] == 0) )
    {
      dataSolidity ++;
      if (checkByte[0] == 0)
      {
        checkByte[0] = MBcode[4];
      } else if (checkByte[1] == 0) {
        checkByte[1] = MBcode[4];
      } else if (checkByte[2] == 0) {
        checkByte[2] = MBcode[4];
      }
      return 0;     // Continue sampling
      
    } else if (dataSolidity > 0 && (checkByte[0] == MBcode[4] || 
                                    checkByte[1] == MBcode[4] || 
                                    checkByte[2] == MBcode[4]) ) {
      dataSolidity ++;
      return 0;     // Continue sampling
      
    } else if (dataSolidity > 0 && (checkByte[0] != MBcode[4] || 
                                    checkByte[1] != MBcode[4] || 
                                    checkByte[2] != MBcode[4]) ) {
      repeats ++;
      if (repeats == 15)
        return 1;   // Cancel/Restart sampling
    }
  }
}

int Nitro_sampling()
{
  int samples[sampleRange] = {0},
      SampleAvg = 0,
      TtlSample = 0;
  int size = 7;
  bool done = false;
  Serial.println("Nitrogen sampling");
  
  for (int s=0; s<sampleRange; s++)
  {
    byte *nitro = Nitrogen();

    if (MBcode_check(nitro, size) == -1)
    {
      s--;
      done = false;
      delay(30);
      continue;
    } else if (MBcode_check(nitro, size) == 1){
      done = false;
      delay(30);
      break;
    } else {
      samples[s] = nitro[4];
      done = true;
      delay(30);
    }
  }

  checkByte[3] = {0};
  repeats = 0;
  dataSolidity = 0;
  
  if (done)
  {
    for (int i=0; i<sampleRange; i++)
    {
      TtlSample = TtlSample + samples[i];
    }
    
    return SampleAvg = TtlSample / sampleRange;
  } else {
    Nitro_sampling();
  }
}

int Phosp_sampling()
{
  int samples[sampleRange] = {0},
      SampleAvg = 0,
      TtlSample = 0;
  int size = 7;
  bool done = false;
  Serial.println("Phosphorous sampling");
  
  for (int s=0; s<sampleRange; s++)
  {
    byte *phosp = Phosphorous();
    
    if (MBcode_check(phosp, size) == -1)
    {
      s--;
      done = false;
      delay(30);
      continue;
    } else if (MBcode_check(phosp, size) == 1){
      done = false;
      delay(30);
      break;
    } else {
      samples[s] = phosp[4];
      done = true;
      delay(30);
    }
  }

  checkByte[3] = {0};
  repeats = 0;
  dataSolidity = 0;
  
  if (done)
  {
    for (int i=0; i<sampleRange; i++)
    {
      TtlSample = TtlSample + samples[i];
    }
    
    return SampleAvg = TtlSample / sampleRange;
  } else {
    Phosp_sampling();
  }
}

int Potas_sampling()
{
  int samples[sampleRange] = {0},
      SampleAvg = 0,
      TtlSample = 0;
  int size = 7;
  bool done = false;
  Serial.println("Potassium sampling");
  
  for (int s=0; s<sampleRange; s++)
  {
    byte *potas = Potassium();
    
    if (MBcode_check(potas, size) == -1)
    {
      s--;
      done = false;
      delay(30);
      continue;
    } else if (MBcode_check(potas, size) == 1){
      done = false;
      delay(30);
      break;
    } else {
      samples[s] = potas[4];
      done = true;
      delay(30);
    }
  }

  checkByte[3] = {0};
  repeats = 0;
  dataSolidity = 0;
  
  if (done)
  {
    for (int i=0; i<sampleRange; i++)
    {
      TtlSample = TtlSample + samples[i];
    }
    
    return SampleAvg = TtlSample / sampleRange;
  } else {
    Potas_sampling();
  }
}

float PH_sampling()
{
  float samples[sampleRange] = {0.0},
      SampleAvg = 0.0,
      TtlSample = 0.0;
  int size = 7;
  bool done = false;
  Serial.println("pH sampling");
  
  for (int s=0; s<sampleRange; s++)
  {
    byte *pH = pH_val();

    if (MBcode_check(pH, size) == -1)
    {
      s--;
      done = false;
      delay(30);
      continue;
    } else if (MBcode_check(pH, size) == 1){
      done = false;
      delay(30);
      break;
    } else {
      samples[s] = pH[4];
      done = true;
      delay(30);
    }
  }

  checkByte[3] = {0};
  repeats = 0;
  dataSolidity = 0;
  
  if (done)
  {
    for (int i=0; i<sampleRange; i++)
    {
      TtlSample = TtlSample + samples[i];
    }
    SampleAvg = TtlSample / sampleRange;
    
    return SampleAvg / 10;
  } else {
    PH_sampling();
  }
}

float Temp_sampling()
{
  float samples[sampleRange] = {0.0},
      SampleAvg = 0.0,
      TtlSample = 0.0;
  Serial.println("Temperature sampling");

  for (int s=0; s<sampleRange; s++)
  {
    float temp = Temp();
    if (temp == -127)
    {
      s--;
      delay(30);
      continue;
    }
    samples[s] = temp;
    delay(30);
  }

  for (int i=0; i<sampleRange; i++)
  {
    TtlSample = TtlSample + samples[i];
  }
    
  return SampleAvg = TtlSample / sampleRange;
}

void Setup_GPRS()
{
  http.init();

  Serial.print("Set Phone Function... ");
  Serial.println(http.setPhoneFunc(1));
  
  Serial.println("GPRS Init.");
  http.gprsInit("internet.mtn");
  
  Serial.print("Close GPRS... ");
  Serial.println(http.closeConn());
  
  Serial.print("Connect GPRS... ");
  Serial.println(http.connect());
  
}

int GSM_post(String url)
{
  String ackRx = "";
  
  if (http.isConnected())
  {
    Serial.println("GPRS connected!");
    ackRx = http.getWithSSL(url);
  } else {
    Serial.println("GPRS not connected!");
    http.reset(); // reset the GSM
    Setup_GPRS(); // restart the GSM
    
    GSM_post(url);
  }

  Serial.println(ackRx);

}

void setup() {
  Serial.begin(115200);
  NPKmb.begin(9600);
  PHmb.begin(9600);
  sensors.begin();
  Serial1.begin(115200);
  while(!Serial1)
  {
    // wait for the GSM module to connect
  }
  

  Setup_GPRS();
  
  pinMode(npk_RE, OUTPUT);
  pinMode(npk_DE, OUTPUT);
  
  pinMode(ph_RE, OUTPUT);
  pinMode(ph_DE, OUTPUT);

  Serial.println("***************************************************************");
  Serial.println("*****                  Soil Nutrient data                 *****");
  Serial.println("***************************************************************");
  Serial.println("Nitrogen      Phosphorous     Potassium      pH     Temperature");
  Serial.println(" mg/kg           mg/kg          mg/kg                    C");
  //Serial.println("   5              4             24         4.56        27.40");
  delay(100);
}

void loop() {
  int npk1, npk2, npk3;
  float Temp, pH;
  char npkStr1[4] = {0}, 
       npkStr2[4] = {0}, 
       npkStr3[4] = {0}, 
       TempStr[11] = {0}, 
       pHstr[11] = {0};
  char postData[80];
  
  /*************************************************************************** 
   *  https://api.thingspeak.com/update?api_key=2W5WQS9K4J8R5KBX&field1=255 
   *  https://api.thingspeak.com/update?api_key=2W5WQS9K4J8R5KBX&field2=255 
   *  https://api.thingspeak.com/update?api_key=2W5WQS9K4J8R5KBX&field3=255 
   *  https://api.thingspeak.com/update?api_key=2W5WQS9K4J8R5KBX&field4=8.90 
   *  https://api.thingspeak.com/update?api_key=2W5WQS9K4J8R5KBX&field5=27.00 
   ***************************************************************************/

  NPKmb.listen();
  delay(50);
  npk1 = Nitro_sampling();
  sprintf(npkStr1, "%d", npk1);
  delay(20);
  npk2 = Phosp_sampling();
  sprintf(npkStr2, "%d", npk2);
  delay(20);
  npk3 = Potas_sampling();
  sprintf(npkStr3, "%d", npk3);
  delay(20);
  
  PHmb.listen();
  delay(50);
  pH = PH_sampling();
  dtostrf(pH, 2, 2, pHstr);
  delay(20);
  Temp = 23.32;//Temp_sampling();
  dtostrf(Temp, 2, 2, TempStr);
  delay(20);
  
  //strcpy(postData, URL);
  //strcat(postData, "&field1=");
  //strcat(postData, npkStr1);
  //strcat(postData, "&field2=");
  //strcat(postData, npkStr2);
  //strcat(postData, "&field3=");
  //strcat(postData, npkStr3);
  //strcat(postData, "&field4=");
  //strcat(postData, pHstr);
  //strcat(postData, "&field5=");
  //strcat(postData, TempStr);
  
  //GSM_post(postData);
  
  String PostData = URL;
  PostData += "&field1=";
  PostData += npkStr1;
  PostData += "&field2=";
  PostData += npkStr2;
  PostData += "&field3=";
  PostData += npkStr3;
  PostData += "&field4=";
  PostData += pHstr;
  PostData += "&field5=";
  PostData += TempStr;

  GSM_post(PostData);


  delay(100);
}
