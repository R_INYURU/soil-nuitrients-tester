#include <SoftwareSerial.h>

#define MB_RE 5
#define MB_DE 4
SoftwareSerial modbus(12, 9);    // RO, DI

unsigned char MB_register[7] = {0};
int MBset = 4;

unsigned char *Temperature(unsigned char *temp, int size)
{
  digitalWrite(MB_DE, HIGH);
  digitalWrite(MB_RE, HIGH);
  
  if(modbus.write(temp, size) == 8)
  {
    digitalWrite(MB_DE, LOW);
    digitalWrite(MB_RE, LOW);
    for(unsigned char i=0; i<7; i++)
    {
      MB_register[i] = modbus.read();
      Serial.print(MB_register[i], HEX);
      Serial.print(" ");
    }
    Serial.println();
  }
  
  return MB_register;
}

unsigned char *read_modbus(unsigned char *registerCode, int size)
{
  int i;

  digitalWrite(MB_RE, HIGH);
  digitalWrite(MB_DE, HIGH);

  if (modbus.write(registerCode, size) == 8)
  {
    digitalWrite(MB_RE, LOW);
    digitalWrite(MB_DE, LOW);

    for (i=0; i<sizeof(MB_register); i++)
    {
      MB_register[i] = modbus.read();
      Serial.print(MB_register[i], HEX);
      Serial.print(" ");
    }
    Serial.println();
  }

  return MB_register;
}

unsigned int calc_crc16(unsigned char *rgster, int size)
{
  int i, j;
  unsigned int c, crc = 0xFFFF;

  for (i=0; i<size; i++)
  {
    c = rgster[i] & 0x00FF;
    crc ^= c;

    for (j=0; j<8; j++)
    {
      if (crc & 0x0001)
      {
        crc >>= 1;
        crc ^= 0xA001;
      } else {
        crc >>= 1;
      }
    }
  }
  
  return crc;
}

void setup() {
  Serial.begin(115200);

  modbus.begin(9600);
  
  pinMode(MBset, INPUT);
  pinMode(MB_DE, OUTPUT);
  pinMode(MB_RE, OUTPUT);
}

void loop() {
  unsigned char *Val;
  float temp = 0;
  int nitro = 0;
  
  unsigned char request[] = {0x01, 0x03, 0x00, 0x0D, 0x00, 0x01, 0x00, 0x00};
  unsigned int crc16_ask = 0;

///////////////////// Request CRC /////////////////////////////// 
  crc16_ask = calc_crc16(request, sizeof(request) - 2);
  
  request[6] = crc16_ask%256;
  request[7] = crc16_ask/256;

  Serial.print("ASK CRC: ");
  if (crc16_ask == 0)
  {
    Serial.println(">>FAILED!<<");
  } else {    
    Serial.print("[");
    Serial.print(request[6], HEX);
    Serial.print(" ");
    Serial.print(request[7], HEX);
    Serial.print("] ");
    for (int i=0; i<8; i++)
    {
      Serial.print(request[i]);
      Serial.print(" ");
    }
    Serial.println();
  }
//////////////////// Response CRC ///////////////////////////////
  unsigned int crc16_rsp = 0;
  
  Val = read_modbus(request, sizeof(request));
  
  crc16_rsp = calc_crc16(request, 8);

  Serial.print("RSP CRC: ");
  if (crc16_rsp == 0)
  {
    for (int i=0; i<7; i++)
    {
      Serial.print(Val[i]);
      Serial.print(" ");
    }
    Serial.println(); 
  } else {
    Serial.print("[");
    Serial.print(crc16_rsp);
    Serial.print("]");
    Serial.println("RESPONSE CRC FAILED!");
  }
//////////////////// Printing Output ////////////////////////////
  temp = float ((Val[3] * 256) + Val[4]) / 100;
  nitro = int(Val[4]);

  Serial.print("Soil temp: ");
  Serial.println(temp);
  Serial.println("********************");

  delay(1000);
}
