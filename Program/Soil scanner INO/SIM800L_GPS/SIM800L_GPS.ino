#include <GSMSimLocation.h>

#define RST_PIN 3

GSMSimLocation GPS(Serial1, RST_PIN);

void setup() {
  Serial1.begin(115200);
  while(!Serial1)
  {
    ;
  }
  Serial.begin(115200);

  GPS.init();

  Serial.print("Set Phone Function... ");
  Serial.println(GPS.setPhoneFunc(1));

  Serial.print("Connect GPRS... ");
  Serial.println(GPS.connect());

  Serial.print("Calculating location...");
  bool calcLoc = GPS.calculateLocation();
  Serial.println(calcLoc);

  if (calcLoc)
  {
    Serial.print("Fetching location code...");
    String LocCode = GPS.getLocationCode();
    Serial.println(LocCode);

    int locCode = LocCode.toInt();
    if (locCode == 0)
    {
      Serial.print("Longitude: ");
      Serial.println(GPS.getLongitude());

      Serial.print("Latitude: ");
      Serial.println(GPS.getLatitude());
    } else {
      Serial.print("Location code not found... ");
      Serial.println(LocCode);
    }
  }
}

void loop() {
  // put your main code here, to run repeatedly:

}
