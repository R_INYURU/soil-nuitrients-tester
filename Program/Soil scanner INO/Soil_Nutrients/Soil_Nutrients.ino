/****************************************************************************************
 * Not all pins on the Mega and Mega 2560 support change interrupts,
 * so only the following can be used for RX:
 * 10, 11, 12, 13, 50, 51, 52, 53, 62, 63, 64, 65, 66, 67, 68, 69
 * 
 ***************************************************************************************/

#include <SoftwareSerial.h>
#include <TinyGPS.h>
#include <GSMSimHTTP.h>
#include <LiquidCrystal.h>

#define TRUE  1
#define FALSE 0

#define SERIAL 1
#if SERIAL
  #define RESULT 1
  #define PRINT 0
  #define DEBUG 0
#endif


#define EC_RE     7
#define EC_DE     6
#define PH_RE     5
#define PH_DE     4
#define NPK_RE    3
#define NPK_DE    2
#define GSM_RST   22
#define GPS_RX    62  // A7
#define GPS_TX    63  // A6
#define btnUP     40
#define btnSLT    42
#define btnDWN    44

const int rs = 38, en = 39, d4 = 32, d5 = 33, d6 = 34, d7 = 35;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

SoftwareSerial ECmb(13, 10);    // RO, DI
SoftwareSerial PHmb(12, 9);   //RO, DI
SoftwareSerial NPKmb(11, 8);    //RO, DI

GSMSimHTTP http(Serial1, GSM_RST);

TinyGPS gps;
SoftwareSerial GPS(GPS_RX, GPS_TX);

unsigned char PH_REG[] = {0x01, 0x03, 0x00, 0x0D, 0x00, 0x01, 0x00, 0x00}; // float

unsigned char NITRO[]  = {0x01, 0x03, 0x00, 0x1E, 0x00, 0x01, 0x00, 0x00}; // int
unsigned char PHOSP[]  = {0x01, 0x03, 0x00, 0x1F, 0x00, 0x01, 0x00, 0x00}; // int
unsigned char POTAS[]  = {0x01, 0x03, 0x00, 0x20, 0x00, 0x01, 0x00, 0x00}; // int

unsigned char TEMP[]  = {0x01, 0x04, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00}; // float
unsigned char VWC[]   = {0x01, 0x04, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00}; // float
unsigned char EC[]    = {0x01, 0x04, 0x00, 0x02, 0x00, 0x01, 0x00, 0x00}; // int
unsigned char SALINITY[]  = {0x01, 0x04, 0x00, 0x03, 0x00, 0x01, 0x00, 0x00}; // int
unsigned char TDS[]       = {0x01, 0x04, 0x00, 0x04, 0x00, 0x01, 0x00, 0x00}; // int
unsigned char EPSILON[]   = {0x01, 0x04, 0x00, 0x05, 0x00, 0x01, 0x00, 0x00}; // float
unsigned char SOIL_TYPE[] = {0x01, 0x03, 0x00, 0x20, 0x00, 0x01, 0x00, 0x00}; // int
unsigned char TEMP_UNIT[] = {0x01, 0x03, 0x00, 0x21, 0x00, 0x01, 0x00, 0x00}; // int

const char URL[] = "api.thingspeak.com/update?api_key=3AJLC4UXGERHYQTX";

int sampleRange = 50;
unsigned char rtnRegister[7] = {0};
int EC_set = 23;

enum SensorTypes
{
  EC_SENSOR = 1,
  PH_SENSOR = 2,
  NPK_SENSOR = 3
};

enum NutrientTypes
{
  ph = 0,
  nitrogen,
  phosphorous,
  potassium,
  temp,
  vwc,
  ec,
  salinity,
  tds,
  epsilon,
};

enum SoilTypes
{
  mineralSoil = 0,
  sandySoil = 1,
  clay = 2,
  organicSoil = 3
};

enum TempUnit
{
  celcius = 0,
  Fahrenheit = 1
};

struct reqRegister
{
  uint8_t phCode        : 1;
  uint8_t nitroCode     : 1;
  uint8_t phospCode     : 1;
  uint8_t potasCode     : 1;
  uint8_t tempCode      : 1;
  uint8_t vwcCode       : 1;
  uint8_t ecCode        : 1;
  uint8_t salinityCode  : 1;
  uint8_t tdsCode       : 1;
  uint8_t epsilonCode   : 1;
  uint8_t soilTypeCode  : 1;
  uint8_t tempUnitCode  : 1;
}reqRgsterFlag;

/****************************************************************************************
 *                                       setup
 * Descr: initialising statements for the system.
 * 
 * Param: None.
 * 
 * return: None.
 ***************************************************************************************/
void setup()
{
  if (SERIAL)
  {
    Serial.begin(115200);
  }
  
  lcd.begin(16, 2);

  NPKmb.begin(9600);
  PHmb.begin(9600);
  ECmb.begin(9600);

  Serial1.begin(115200);
  while(!Serial1)
  {
    // wait for the GSM module to connect
  }
  
  pinMode(EC_set, INPUT);
  pinMode(EC_DE, OUTPUT);
  pinMode(EC_RE, OUTPUT);
  pinMode(PH_DE, OUTPUT);
  pinMode(PH_RE, OUTPUT);
  pinMode(NPK_DE, OUTPUT);
  pinMode(NPK_RE, OUTPUT);
  
  digitalWrite(EC_set, LOW);

  /* Initializing Flags code */
  reqRgsterFlag.phCode       = FALSE;
  reqRgsterFlag.nitroCode    = FALSE;
  reqRgsterFlag.phospCode    = FALSE;
  reqRgsterFlag.potasCode    = FALSE;
  reqRgsterFlag.tempCode     = FALSE;
  reqRgsterFlag.vwcCode      = FALSE;
  reqRgsterFlag.ecCode       = FALSE;
  reqRgsterFlag.salinityCode = FALSE;
  reqRgsterFlag.tdsCode      = FALSE;
  reqRgsterFlag.epsilonCode  = FALSE;
  reqRgsterFlag.soilTypeCode = FALSE;
  reqRgsterFlag.tempUnitCode = FALSE;

  enum SensorTypes SoilSensor;
  enum NutrientTypes SoilNutrients;
  enum SoilTypes soilTypes;
  enum TempUnit tempUnit;

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Soil Nuitrient");
  lcd.setCursor(1, 1);
  lcd.print("Test Instrument");

  // Setup_GPRS();
  
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Initialising...");

  Calibration();
  if (RESULT)
  { 
    Serial.println("****************************************************************************************");
    Serial.println("*****                              Soil Nutrient Test                              *****");
    Serial.println("****************************************************************************************");
    Serial.println("| Nitro | Phosp | Potas |  pH  | Temp  | VWC |   EC  | Salt | TDS | EPSILON | SoilType |");
    Serial.println("| mg/kg | mg/kg | mg/kg |      | C     |  %  | us/cm | mg/L | mg/L| constant|          |");
// Serial.println("|   5   | 4     | 24    | 7.26 | 24.56 | 36  | 27.40 | 1821 |1821 |  70.23  | organic  |");
  }

}

void loop()
{ 
  unsigned char *soilType;
  int respCheck;
  char phStr[6],
       nitroStr[6],
       phospStr[6],
       potasStr[6],
       tempStr[6],
       vwcStr[6],
       ecStr[6],
       saltStr[6],
       tdsStr[6],
       epsStr[6],
       soilStr[8];

  float phVal = Float_sampling(ph);
  int nitroVal = Int_sampling(nitrogen);
  int phospVal = Int_sampling(phosphorous);
  int potasVal = Int_sampling(potassium);
  float tempVal = Float_sampling(temp);
  float vwcVal = Float_sampling(vwc);
  int ecVal = Int_sampling(ec);
  int saltVal = Int_sampling(salinity);
  int tdsVal = Int_sampling(tds);
  float epsVal = Float_sampling(epsilon);

  if (RESULT)
  {
    Serial.print("|   ");
    Serial.print(nitroVal);
    Serial.print("   |   ");
    Serial.print(phospVal);
    Serial.print("   |  ");
    Serial.print(potasVal);
    Serial.print("   | ");
    Serial.print(phVal);
    Serial.print(" | ");
    Serial.print(tempVal);
    Serial.print(" | ");
    Serial.print(vwcVal);
    Serial.print("  | ");
    Serial.print(ecVal);
    Serial.print(" | ");
    Serial.print(saltVal);
    Serial.print(" | ");
    Serial.print(tdsVal);
    Serial.print(" |  ");
    Serial.print(epsVal);
    Serial.print("  | ");
  }
  soilType = EC_read_register(SOIL_TYPE, sizeof(SOIL_TYPE));
  respCheck = Response_CRC(soilType, sizeof(SOIL_TYPE) - 1);
  if (respCheck == TRUE)
  {
    if (RESULT)
    {
      switch (soilType[4])
      {
      case 0:
        Serial.print("mineral");
        strcpy(soilStr, "mineral");
        break;
      case 1:
        Serial.print("sandy");
        strcpy(soilStr, "sandy");
        break;
      case 2:
        Serial.print("clay");
        strcpy(soilStr, "clay");
        break;
      case 3:
        Serial.print("organic");
        strcpy(soilStr, "organic");
        break;
      default:
        Serial.print("Unknown");
        strcpy(soilStr, "Unknown");
      }
      Serial.println("  |");

    }
  }
  
  dtostrf(phVal, 2, 2, phStr);
  sprintf(nitroStr, "%d", nitroVal);
  sprintf(phospStr, "%d", phospVal);
  sprintf(potasStr, "%d", potasVal);
  dtostrf(tempVal, 2, 2, tempStr);
  dtostrf(vwcVal, 2, 2, vwcStr);
  sprintf(ecStr, "%d", ecVal);
  sprintf(saltStr, "%d", saltVal);
  sprintf(tdsStr, "%d", tdsStr);
  dtostrf(epsVal, 2, 2, epsStr);

  String PostData = URL;
         PostData += "&field1=";
         PostData += nitroStr;
         PostData += "&field2=";
         PostData += phospStr;
         PostData += "&field3=";
         PostData += potasStr;
         PostData += "&field4=";
         PostData += phStr;
         PostData += "&field5=";
         PostData += tempStr;
         PostData += "&field6=";
         PostData += vwcStr;
         PostData += "&field7=";
         PostData += ecStr;
         PostData += "&field8=";
         PostData += saltStr;

  // GSM_post(PostData);

  delay(100);
}

/****************************************************************************************
 *                                       Setup_GPRS
 * Descr: initialising statements for the GSM are in here.
 * 
 * Param: None.
 * 
 * return: None.
 ***************************************************************************************/
void Setup_GPRS()
{
  http.init();

  if (DEBUG)
  {
    Serial.print("Set Phone Function... ");
    Serial.println(http.setPhoneFunc(1));
    
    Serial.println("GPRS Init.");
    http.gprsInit("internet.mtn");
    
    Serial.print("Close GPRS... ");
    Serial.println(http.closeConn());
    
    Serial.print("Connect GPRS... ");
    Serial.println(http.connect());

  } else {
    http.setPhoneFunc(1);
    http.gprsInit("internet.mtn");

    http.closeConn();
    http.connect();
  }
}


/****************************************************************************************
 *                                  GSM_post func.
 * Descr: Sending/Posting data to the Web.
 * 
 * Parm: url[String] - It hold the full length url to be used with post command.
 * 
 * return: None.
 ***************************************************************************************/
int GSM_post(String url)
{
  String ackRx = "";
  
  if (http.isConnected())
  {
    Serial.println("GPRS connected!");
    ackRx = http.getWithSSL(url);
  } else {
    Serial.println("Warning: GPRS not connected!");
    http.reset();
    Setup_GPRS();
    
    GSM_post(url);
  }

  Serial.println(ackRx);

}

/****************************************************************************************
 *                                      Int_sampling
 * Descr: sampling data from sensors outputing interger value.
 * 
 * Param: nutrientType[int].
 * 
 * return: sampled value.
 ***************************************************************************************/
int Int_sampling(int nutrientType)
{
  int samples[sampleRange] = {0},
      sampleAvg = 0,
      TtlSample = 0,
      failCounts = 0,
      respCheck;
  
  unsigned char *nutrient;
  unsigned char *bandRegister;

  for (int s=0; s<sampleRange; s++)
  {
    switch (nutrientType)
    {
      case nitrogen:
        NPKmb.listen();
        if (NPKmb.isListening())
        {
          nutrient = NPK_read_register(NITRO, sizeof(NITRO));
          respCheck = Response_CRC(nutrient, sizeof(NITRO) - 1);
        }
        break;
      case phosphorous:
        NPKmb.listen();
        if (NPKmb.isListening())
        {
          nutrient = NPK_read_register(PHOSP, sizeof(PHOSP));
          respCheck = Response_CRC(nutrient, sizeof(PHOSP) - 1);
        }      
        break;
      case potassium:
        NPKmb.listen();
        if (NPKmb.isListening())
        {
          nutrient = NPK_read_register(POTAS, sizeof(POTAS));
          respCheck = Response_CRC(nutrient, sizeof(POTAS) - 1);
        }
        break;
      case ec:
        ECmb.listen();
        if (ECmb.isListening())
        {
          nutrient = EC_read_register(EC, sizeof(EC));
          respCheck = Response_CRC(nutrient, sizeof(EC) - 1);
        }
        break;
      case salinity:
        ECmb.listen();
        if (ECmb.isListening())
        {
          nutrient = EC_read_register(SALINITY, sizeof(SALINITY));
          respCheck = Response_CRC(nutrient, sizeof(SALINITY) - 1);
        }
        break;
      case tds:
        ECmb.listen();
        if (ECmb.isListening())
        {
          nutrient = EC_read_register(TDS, sizeof(TDS));
          respCheck = Response_CRC(nutrient, sizeof(TDS) - 1);
        }      
        break;
    }

    if (respCheck == TRUE)
    {
      samples[s] = int((nutrient[3] * 256) + nutrient[4]);
      smart_delay(30);
    } else {
      s--;
      failCounts ++;
      if (failCounts >= (sampleRange/3))
      {
        return -13;
      }
      smart_delay(30);
      continue;
    }
  }

  for(int i=0; i<sampleRange; i++)
  {
    TtlSample = TtlSample + samples[i];
  }

  return sampleAvg = TtlSample / sampleRange;
}

/****************************************************************************************
 *                                      Float_sampling
 * Descr: sampling data from sensors outputing float values.
 * 
 * Param: nutrientType[int].
 * 
 * return: sampled value.
 ***************************************************************************************/
float Float_sampling (int nutrientType)
{
  float samples[sampleRange] = {0.0},
        sampleAvg = 0.0,
        TtlSample = 0.0;
  
  int failCounts = 0,
      respCheck;
  unsigned char *nutrient;
  unsigned char *bandRegister;

  for (int s=0; s<sampleRange; s++)
  {
    switch (nutrientType)
    {
      case ph:
        PHmb.listen();
        if (PHmb.isListening())
        {
          nutrient = pH_read_register(PH_REG, sizeof(PH_REG));
          respCheck = Response_CRC(nutrient, sizeof(PH_REG) - 1);
        }      
        break;
      case temp:
        ECmb.listen();
        if (ECmb.isListening())
        {
          nutrient = EC_read_register(TEMP, sizeof(TEMP));
          respCheck = Response_CRC(nutrient, sizeof(TEMP) - 1);
        }
        break;
      case vwc:
        ECmb.listen();
        if (ECmb.isListening())
        {
          nutrient = EC_read_register(VWC, sizeof(VWC));
          respCheck = Response_CRC(nutrient, sizeof(VWC) - 1);
        }      
        break;
      case epsilon:
        ECmb.listen();
        if (ECmb.isListening())
        {
          nutrient = EC_read_register(EPSILON, sizeof(EPSILON));
          respCheck = Response_CRC(nutrient, sizeof(EPSILON) - 1);
        }      
        break;
    }

    if (respCheck == TRUE)
    {
      if (nutrientType == ph)
      {
        samples[s] = float((nutrient[3] * 256) + nutrient[4]) / 10;
      } else {
        samples[s] = float((nutrient[3] * 256) + nutrient[4]) / 100;
      }
      smart_delay(30);
    } else {
      s--;
      failCounts ++;
      if (failCounts >= (sampleRange/3))
      {
        return -14;
      }
      smart_delay(30);
      continue;
    }
  }

  for(int i=0; i<sampleRange; i++)
  {
    TtlSample = TtlSample + samples[i];
  }

  return sampleAvg = TtlSample / sampleRange;
}

/****************************************************************************************
 *                                Calibration func.
 * Descr: calculate the CRC check register for the register code of all sensors.
 * 
 * param: None.
 * 
 * return: None.
 ***************************************************************************************/
void Calibration()
{
  /* Calibrating EC sensor */
  ECmb.listen();
  smart_delay(50);
  if (ECmb.isListening())
  {
    reqRgsterFlag.tempCode = Request_CRC(TEMP, sizeof(TEMP), EC_SENSOR);
    reqRgsterFlag.tempCode ? Serial.println("###Temperature CRC check True.###") : Serial.println("===>Temperature CRC incorrect!<===");
    reqRgsterFlag.vwcCode  = Request_CRC(VWC, sizeof(VWC), EC_SENSOR);
    reqRgsterFlag.vwcCode ? Serial.println("###VWC CRC check True.###") : Serial.println("===>VWC CRC incorrect!<===");
    reqRgsterFlag.ecCode   = Request_CRC(EC, sizeof(EC), EC_SENSOR);
    reqRgsterFlag.ecCode ? Serial.println("###EC CRC check True.###") : Serial.println("===>EC CRC incorrect!<===");
    reqRgsterFlag.salinityCode = Request_CRC(SALINITY, sizeof(SALINITY), EC_SENSOR);
    reqRgsterFlag.salinityCode ? Serial.println("###Salinity CRC check True.###") : Serial.println("===>Salinity CRC incorrect!<===");
    reqRgsterFlag.tdsCode = Request_CRC(TDS, sizeof(TDS), EC_SENSOR);
    reqRgsterFlag.tdsCode ? Serial.println("###TDS CRC check True.###") : Serial.println("===>TDS CRC incorrect!<===");
    reqRgsterFlag.epsilonCode = Request_CRC(EPSILON, sizeof(EPSILON), EC_SENSOR);
    reqRgsterFlag.epsilonCode ? Serial.println("###Epsilon CRC check True.###") : Serial.println("===>Epsilon CRC incorrect!<===");
    reqRgsterFlag.soilTypeCode = Request_CRC(SOIL_TYPE, sizeof(SOIL_TYPE), EC_SENSOR);
    reqRgsterFlag.epsilonCode ? Serial.println("###SoilType CRC check True.###") : Serial.println("===>SoilType CRC incorrect!<===");
    reqRgsterFlag.tempUnitCode = Request_CRC(TEMP_UNIT, sizeof(TEMP_UNIT), EC_SENSOR);
    reqRgsterFlag.tempUnitCode ? Serial.println("###TempUnit CRC check True.###") : Serial.println("===>TempUnit CRC incorrect!<===");
  }
  
  /* Calibrating pH sensor */
  PHmb.listen();
  smart_delay(50);
  if (PHmb.isListening())
  {
    reqRgsterFlag.phCode = Request_CRC(PH_REG, sizeof(PH_REG), PH_SENSOR);
    reqRgsterFlag.phCode ? Serial.println("###pH CRC check True.###") : Serial.println("===>pH CRC incorrect!<==="); 
  }
  
  /* Calibrating NPK sensor */
  NPKmb.listen();
  smart_delay(50);
  if (NPKmb.isListening())
  {
    reqRgsterFlag.nitroCode = Request_CRC(NITRO, sizeof(NITRO), NPK_SENSOR);
    reqRgsterFlag.nitroCode ? Serial.println("###Nitrogen CRC check True.###") : Serial.println("===>Nitrogen CRC incorrect!<===");
    reqRgsterFlag.phospCode = Request_CRC(PHOSP, sizeof(PHOSP), NPK_SENSOR);
    reqRgsterFlag.phospCode ? Serial.println("###Phosphorous CRC check True.###") : Serial.println("===>Phosphorous CRC incorrect!<===");
    reqRgsterFlag.potasCode = Request_CRC(POTAS, sizeof(POTAS), NPK_SENSOR);
    reqRgsterFlag.potasCode ? Serial.println("###Potassium CRC check True.###") : Serial.println("===>Potassium CRC incorrect!<==="); 
  }
  
}

/****************************************************************************************
 *                              calculate_crc16 func.
 * param:  registerCode[unsigned char *] - This code will be either request format of    *         the response format.
 *         size[int] - The size of the register code we are checking.
 * 
 * return: crc[unsigned int]  - calculated value for crc16 is the check value returned. 
 ***************************************************************************************/
unsigned int Calculate_crc16(unsigned char *registerCode, int size)
{
  int i, j;
  unsigned int c, crc = 0xFFFF;

  for (i=0; i<size; i++)
  {
    c = registerCode[i] & 0x00FF;
    crc ^= c;

    for (j=0; j<8; j++)
    {
      if (crc & 0x0001)
      {
        crc >>= 1;
        crc ^= 0xA001;
      } else {
        crc >>= 1;
      }
    }
  }

  return crc;
}

/****************************************************************************************
 *                              Response_CRC
 * param: registerCode[unsigned char *] - This code will be either request format of    *        the response format.
 *        size[int] - The size of the register code we are checking.
 * 
 * return: TRUE if the calculated crc16 returns 0 or FALSE otherwise. 
 **************************************************************************************/
int Response_CRC(unsigned char *registerCode, int size)
{
  unsigned int crc16 = 1;
  
  crc16 = Calculate_crc16(registerCode, size);

  if (crc16 == 0)
  {

    return TRUE;
  } else {
    if (DEBUG)
    {
      Serial.print("Error: RSP crc failed w/ = ");
      Serial.println(crc16);
    }
    
    return FALSE;
  }
}

/****************************************************************************************
 *                          Request_CRC func.
 * param:  registerCode[unsigned char *] - This code will be either request format of    *         the response format.
 *         size[int] - The size of the register code we are checking.
 * 
 * return: TRUE if the calculated crc16 value is different from 0 and can be used or
 *         FALSE if the calculated crc16 is equal to 0. 
 ***************************************************************************************/
int Request_CRC(unsigned char *registerCode, int size, int sensorType)
{
  int i;
  unsigned int crc16 = 0;
  unsigned char *bandRegister;

  crc16 = Calculate_crc16(registerCode, size - 2);

  if (crc16 == 0)
  {
    if (DEBUG)
    {
      Serial.println("Error: in the Request register's code!");
    }
    
    return FALSE;
  } else {
    registerCode[6] = crc16%256;
    registerCode[7] = crc16/256;
    
    for (i=0; i<5; i++)
    {
      if (sensorType == EC_SENSOR)
      {
        bandRegister = EC_read_register(registerCode, size);
        
        if (Response_CRC(bandRegister, size - 1) == TRUE)
          return TRUE;
        else
          smart_delay(30);
          continue;

      } else if (sensorType == PH_SENSOR) {
        bandRegister = pH_read_register(registerCode, size);
        
        if (Response_CRC(bandRegister, size - 1) == TRUE)
          return TRUE;
        else
          smart_delay(30);
          
          continue;
        
      } else if (sensorType == NPK_SENSOR) {
        bandRegister = NPK_read_register(registerCode, size);

        if (Response_CRC(bandRegister, size - 1) == TRUE)
          return TRUE;
        else
          smart_delay(30);
          continue;

      } else {
        if (DEBUG)
        {
          Serial.println("Error: Sensor type selector is not available!!");
        }
      }
    }

    return FALSE;
  }
}

/****************************************************************************************
 *                                EC_read_register
 * param: registerCode[unsigned char *] - This code will be either request format of    *        the response format.
 *        size[int] - The size of the register code we are checking.
 * 
 * return: rtnRegister - Register code read from the address(registerCode) given.
 *************************************************************************************/
unsigned char *EC_read_register(unsigned char *registerCode, int size)
{
  int i;

  digitalWrite(EC_RE, HIGH);
  digitalWrite(EC_DE, HIGH);
  
  if (ECmb.write(registerCode, size) == 8)
  {
    digitalWrite(EC_RE, LOW);
    digitalWrite(EC_DE, LOW);

    for (i=0; i<sizeof(rtnRegister); i++)
    {
      rtnRegister[i] = ECmb.read();
      if (PRINT)
      {
        Serial.print(rtnRegister[i], HEX);
        Serial.print(" ");
      }
    }
    if (PRINT)
    {
      Serial.println();
    }
  }

  return rtnRegister;
}

/****************************************************************************************
 *                                pH_read_register
 * param: registerCode[unsigned char *] - This code will be either request format of    *        the response format.
 *        size[int] - The size of the register code we are checking.
 * 
 * return: rtnRegister - Register code read from the address(registerCode) given.
 *************************************************************************************/
unsigned char *pH_read_register(unsigned char *registerCode, int size)
{
  int i;

  digitalWrite(PH_RE, HIGH);
  digitalWrite(PH_DE, HIGH);
  
  if (PHmb.write(registerCode, size) == 8)
  {
    digitalWrite(PH_RE, LOW);
    digitalWrite(PH_DE, LOW);

    for (i=0; i<sizeof(rtnRegister); i++)
    {
      rtnRegister[i] = PHmb.read();
      if (PRINT)
      {
        Serial.print(rtnRegister[i], HEX);
        Serial.print(" ");
      }
    }
    if (PRINT)
    {
      Serial.println();
    }
  }

  return rtnRegister;
}

/****************************************************************************************
 *                                NPK_read_register
 * param: registerCode[unsigned char *] - This code will be either request format of    
 *        the response format.
 *        size[int] - The size of the register code we are checking.
 * 
 * return: rtnRegister - Register code read from the address(registerCode) given.
 *************************************************************************************/
unsigned char *NPK_read_register(unsigned char *registerCode, int size)
{
  int i;

  digitalWrite(NPK_RE, HIGH);
  digitalWrite(NPK_DE, HIGH);

  if (NPKmb.write(registerCode, size) == 8)
  {
    digitalWrite(NPK_RE, LOW);
    digitalWrite(NPK_DE, LOW);

    for (i=0; i<sizeof(rtnRegister); i++)
    {
      rtnRegister[i] = NPKmb.read();
      if (PRINT)
      {
        Serial.print(rtnRegister[i], HEX);
        Serial.print(" ");
      }
    }
    if (PRINT)
    {
      Serial.println();
    }
  }

  return rtnRegister;
}

/****************************************************************************************
 *                                     smart_delay
 * Descr: instead of stopping the processer for some milli second you can be refreshing 
 * the GPS for new data.
 * 
 * Param: ms[unsigned long].
 * 
 * return: None.
 ***************************************************************************************/
static void smart_delay(unsigned long ms)
{
  unsigned long start = millis();
  do
  {
    while (GPS.available())
      gps.encode(GPS.read());
  } while (millis() - start < ms);
}
