# General Report

### December, 2020

* Joined RSA 

* Read research paper about precision agriculture

* Worked on selecting components for Soil scanner prototype

### January, 2021

* Continued to read and learning more about precision agriculture

* Started prototyping and testing GPS receiver, GSM transmitter, temperature and moisture sensors, ...
  
  ![](./images/proto-3.jpeg)

* Video name (<mark>Feb - prototype.mp4</mark>)

### February, 2021

* Soil sensors (NPK, PH, & EC) arrived and I started calibrating them.

* I started pushing data measured by sensors on [ThingSpeak](https://thingspeak.com/channels/1313581) platform

![](./images/online-1.png)

### March, 2021

* I tested sensors, GPS, and GSM on a breadboard.

![](./images/proto-5.jpg)

![](./images/proto-4.jpg)

* Link to [source code](https://gitlab.com/R_INYURU/soil-nuitrients-tester).

### April, 2021

* I started designing schematic for the PCB(printed circuit board) of the final design
  
  * <u>sensors connectors schematic</u>
    
    ![](./images/pcb-1.png)
  
  * <u>LCD & GSM  schematic design</u>
    
    ![](./images/pcb-2.png)
  
  * <u>GPS receiver schematic</u>
    
    ![](./images/pcb-3.png)
  
  * <u>ATmega2560 microcontroller chip schematic</u>
    
    ![](./images/pcb-4.png)

### May, 2021

* I designed a proposal enclosure of the final product
  
  ![](./images/cover-v1.png)
  
  ![](./images/cover-v2.png)
  
  I started designing shield board for combining all the components the final device will be composed of.
  
  ![](./images/shield-1.jpg)
  
  ![](./images/shield-2.jpg)




